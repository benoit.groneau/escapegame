from .action import Action3
from mud.events import CleanWithEvent

class CleanWithAction(Action3):
    EVENT = CleanWithEvent
    ACTION = "clean-with"
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_operate"