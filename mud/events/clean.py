from .event import Event3


class CleanWithEvent(Event3):
    NAME = "clean-with"

    def perform(self):
        if not self.object.has_prop("cleanable-with"):
            self.fail()
            return self.inform("clean-with.failed")
        if not self.object2.has_prop("cleaner"):
            self.fail()
            return self.inform("clean-with.failed")
        self.inform("clean-with")